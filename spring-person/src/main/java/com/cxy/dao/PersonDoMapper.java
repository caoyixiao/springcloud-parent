package com.cxy.dao;

import com.cxy.dataObject.PersonDo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PersonDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PersonDo record);

    int insertSelective(PersonDo record);

    PersonDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PersonDo record);

    int updateByPrimaryKey(PersonDo record);
}