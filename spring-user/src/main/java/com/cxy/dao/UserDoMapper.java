package com.cxy.dao;

import com.cxy.dataObject.UserDo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserDo record);

    int insertSelective(UserDo record);

    UserDo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserDo record);

    int updateByPrimaryKey(UserDo record);
}