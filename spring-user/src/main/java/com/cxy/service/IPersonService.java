package com.cxy.service;

import com.cxy.dataObject.PersonDo;
import com.cxy.service.impl.PersonServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "cxy-person-service",fallback=PersonServiceImpl.class)
public interface IPersonService {
    @RequestMapping("/person/{id}")
    PersonDo getPersonDoById(@PathVariable("id")Integer id);
}
