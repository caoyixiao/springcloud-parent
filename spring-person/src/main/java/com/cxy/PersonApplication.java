package com.cxy;

        import org.mybatis.spring.annotation.MapperScan;
        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;
        import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
        import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 * @ClassName: PersonApplication
 * @Description:
 * @Auther: 陈绪友
 * @Date: 2019/1/2816:30
 * @version : V1.0
 */
@SpringBootApplication
@EnableEurekaClient  //开启注解，注册服务
@MapperScan("com.cxy.dao")
public class PersonApplication {
    public static void main(String[] args) {
        SpringApplication.run(PersonApplication.class,args);
    }
}
