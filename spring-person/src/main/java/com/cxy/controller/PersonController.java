package com.cxy.controller;

import com.cxy.dao.PersonDoMapper;
import com.cxy.dataObject.PersonDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/***
 * @ClassName: PersonController
 * @Description:
 * @Auther: 陈绪友
 * @Date: 2019/1/2816:31
 * @version : V1.0
 */
@RequestMapping("/person")
@RestController
public class PersonController {
    @Autowired
    private PersonDoMapper personDoMapper;

    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public PersonDo selectPersonDoByid(@PathVariable Integer id){
        return  personDoMapper.selectByPrimaryKey(id);
    }
    @RequestMapping(value = "{id}",method = RequestMethod.DELETE)
    public Integer deletePersonDoByid(@PathVariable Integer id){
        return  personDoMapper.deleteByPrimaryKey(id);
    }

}
