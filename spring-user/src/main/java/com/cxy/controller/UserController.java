package com.cxy.controller;

import com.cxy.dao.UserDoMapper;
import com.cxy.dataObject.PersonDo;
import com.cxy.dataObject.UserDo;
import com.cxy.service.IPersonService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import sun.rmi.runtime.Log;

/***
 * @ClassName: UserController
 * @Description:
 * @Auther: 陈绪友
 * @Date: 2019/1/2817:34
 * @version : V1.0
 */
@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private UserDoMapper userDoMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IPersonService personService;

    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public UserDo selectUserDoById(Long id){
        return  userDoMapper.selectByPrimaryKey(id);
    }

    @RequestMapping(value = "{id}",method = RequestMethod.DELETE)
    public Integer deleteUserDoById(Long id){
        return  userDoMapper.deleteByPrimaryKey(id);
    }
    /**
    * @Author 陈绪友
    * @Description //使用restTemplate进行服务调用
    * @Date  2019/1/28
    * @Param []
    * @return com.cxy.dataObject.PersonDo
    **/
    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public PersonDo sselectPsersonById(){
        PersonDo personDo =restTemplate.getForObject("http://cxy-person-service/person/20",PersonDo.class);
        return personDo;

    }

    /**
    * @Author 陈绪友
    * @Description //使用feign调用远程服务
    * @Date  2019/1/28
    * @Param
    * @return
    **/
   /* @HystrixCommand(fallbackMethod = "fallback",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value="true"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value="10"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value="10000"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value="50"),
    })*/
    @RequestMapping(value = "/get/{id}",method = RequestMethod.GET)
    public PersonDo selectPsersonById(@PathVariable Integer id){
        PersonDo personDo =personService.getPersonDoById(id);
        return personDo;

    }
}
